# vendored_licenses_packager

This [bash](https://www.gnu.org/software/bash)
script may help packagers to include some legal files of  
vendored dependencies in archives created with [RPM](http://rpm.org/).

It was used during build of applications written in  
[Go](https://go.dev/) and 
[Rust](https://www.rust-lang.org/) 
for [openSUSE](https://www.opensuse.org) 
on [OBS](https://build.opensuse.org).

---

### How to use

#### <u>RPM macros</u>

Assuming extracted dependencies are available in directory "vendor"  
below the working directory, the perhaps simplest way to make use of this  
in a RPM spec file is:

**BuildRequires:** `vendored_licenses_packager`

and to add the following macros in their sections:

**%prep**  
`%vendored_licenses_packager_prep`  

**%install**  
`%vendored_licenses_packager_install`

**%files**  
`%vendored_licenses_packager_files`


#### <u>Script</u>

Executing the program directly offers some more flexibilty:

        /path/to/vendored_licenses_packager.sh help

**Be careful:**  
It can delete files recursively without confirmation.

---

### This comes with no guarantee or promise to do what you expect.

#### See it as experimental, untested and unreviewed.

It is currently licensed under the
EUPL v. 1.2 (only) where not noted otherwise.

---

#### [Public upstream repository](https://codeberg.org/cunix/vendored_licenses_packager)

#### Downstream

* [openSUSE](https://build.opensuse.org/package/show/devel:tools:building/vendored_licenses_packager)

#### Spec file examples

 *  [dnscrypt-proxy @ openSUSE](https://build.opensuse.org/package/view_file/server:dns/dnscrypt-proxy/dnscrypt-proxy.spec)

 *  [rage @ openSUSE](https://build.opensuse.org/package/view_file/security/rage-encryption/rage-encryption.spec)

 *  [rsign2 @ openSUSE](https://build.opensuse.org/package/view_file/security/rsign2/rsign2.spec)

---

#### Verify released archives

Currently uses [Minisign](https://jedisct1.github.io/minisign/), 
for example implemented with [rsign2](https://github.com/jedisct1/rsign2).  

To check version "x.y.z" , do:

     rsign verify -P RWQ7vwLW5BF3r8teYKFPJuJb0NTCQi0OEhy3XvyySV78SIvlGE3ofPVx \
                  -x /path/to/vendored_licenses_packager-x.y.z.tar.xz.minisig \
                  /path/to/vendored_licenses_packager-x.y.z.tar.xz
