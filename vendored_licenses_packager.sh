#!/bin/bash

# author: cunix 2019 - 2022
#
# Code signing:
# minisign - RWQ7vwLW5BF3r8teYKFPJuJb0NTCQi0OEhy3XvyySV78SIvlGE3ofPVx
# gpg - 2D79 D9BB 779A 1295 6879 F93E 9C6C BAD7 1E94 9599
#
# Licensed under the EUPL v. 1.2 only since year 2022

version="0.1.0"

disclaimer="
  DISCLAIMER\n
  No promise is made that any obligation stated in license of\n
  vendored source code or in another way will be met or\n
  fulfilled by using this program!\n
  Be careful:\n
  Script can delete files recursively without confirmation.\n\n
  USE AT YOUR OWN RISK!\n
"

legal_file_names="
  copying
  copyright
  legal
  licence
  license
  notice
  patents
  unlicense
"

legal_directory_names="
  licence
  licences
  license
  licenses
"

build_user="abuild"


silent=0
run_tests=""


function helper {
  cat <<EOF
  HOW TO USE:
    Without any guarantee or promise this may be used to try helping
    packagers to include some legal files from vendored source code archives.

    Script has to be run twice in spec file:

    1. in directory "vendor" with vendored dependencies to find files with:

        path_to_script find path_to_tmp_target_directory

      example in section %prep:
        cd vendor
        vendored_licenses_packager find /tmp/vendoring

    2. to install and link found files with:

        path_to_script install path_to_tmp_target_directory path_to_buildroot_target_directory

      example in section %install:
        install -d -m 0755 %{buildroot}%{_licensedir}/%{name}/vendored
        vendored_licenses_packager install /tmp/vendoring %{buildroot}/%{_licensedir}/%{name}/vendored

    3. Files should be packaged:

      example in section %files:
        %{_licensedir}/%{name}/vendored/

  Some more details in source code.

  Bugs are probably included - if found, they can be reported somewhere.

  Upstream: https://codeberg.org/cunix/vendored_licenses_packager
EOF
}


found_files=0
found_dirs=0

function finder {
  # Tries to find and prepare licenses from vendored packages for
  # installation as file or link to existing file.
  #
  # $1 set to "runanyway" allows developers to execute outside build environment
  # $2 set to "cleanup" deletes temporary files
  # $3 tests to run
  # $4 do not show fish
  # $5 should be a destination directory for vendored licenses

  local knows_the_risk=$1
  local cleanup=$2
  local testsrunner=$3
  local donotshowfish=$4
  local vendor_licenses_dir=$5
  local username=$(whoami)
  local workingdir=$(pwd)
  local licenses_file=$(mktemp /tmp/license_files_XXXXXXXXXX.txt)
  local licenses_directories=$(mktemp /tmp/license_dirs_XXXXXXXXXX.txt)
  local real_files=0
  local linked_files=0
  local goahead=0
  local hash_list=()
  local filename_list=()
  local do_no_search=0

  if [[ -z "$vendor_licenses_dir" ]]
    then
      local e12="missing directory as parameter"
      exit_error 12 "$e12"
    else
      if [[ "$vendor_licenses_dir" = "/" ]] \
        || [[ "$vendor_licenses_dir" = "/home" ]] \
        || [[ "$vendor_licenses_dir" = "/home/" ]] \
        || [[ "$vendor_licenses_dir" = "/home/$username" ]] \
        || [[ "$vendor_licenses_dir" = "/home/$username/" ]] \
        || [[ "$vendor_licenses_dir" = "$HOME" ]]
        then
          local e13="Do not use '$vendor_licenses_dir' as destination directory.
                    It will delete all your files."
          exit_error 13 "$e13"
        else
          local p1=$(user_check $knows_the_risk)
          if [[ $p1 -eq 1 ]]
            then
              delete_dir_recursive "$knows_the_risk" "$vendor_licenses_dir"
              local _mdir=$(mkdir -pv "$vendor_licenses_dir")
              if [[ -d "$vendor_licenses_dir" ]]
                then
                  echoer "created: $vendor_licenses_dir"
                  echoer Searching for legal files ...
                  rm $licenses_file $licenses_directories
                  local has_directories=$(find ./* -type d)
                  if [[ -z "$has_directories" ]]
                    then
                      echoer "No directories found to search beneath $workingdir"
                      do_no_search=1
                    else
                      for item in $legal_file_names
                        do
                          echoer "searching for file names starting with $item"
                          items_finder  \
                              "f" \
                              "$licenses_file" \
                              "$item" \
                              $found_files
                        done
                      for item in $legal_directory_names
                        do
                          echoer "searching for directory names starting with $item"
                          items_finder \
                              "d" \
                              "$licenses_directories" \
                              "$item" \
                              $found_dirs
                        done
                      if [[ -s "$licenses_directories" ]]
                        then
                          while read line
                            do
                              local dish=$(fisher "$line")
                              if [[ 0 -eq "$dish" ]]
                                then
                                  if [[ -d "$line" ]]
                                    then
                                      echoer "looking into directory : $line"
                                      items_finder \
                                          "fd" \
                                          "$licenses_file" \
                                          "$line" \
                                          $found_files
                                    else
                                      echoer "invalid directory: $line"
                                    fi
                                else
                                  if [[ "$donotshowfish" != "donotshowfish" ]]
                                    then
                                      echoer "no, thanks for the dish$dish : $line"
                                    fi
                                fi
                            done < $licenses_directories
                        fi
                    fi
                  goahead=1
                else
                  local e14="$vendor_licenses_dir is not a directory."
                  exit_error 14 "$e14"
                fi
            else
              local e15="Script should only be executed in build environment
                        indicated by user '$build_user'."
              exit_error 15 "$e15"
            fi
        fi
    fi

  if [[ "$testsrunner" == *"-fisher1"* ]]
    then
      echoer "adding bogus test files ..."
      test_fisher1 "$licenses_file"
    fi
  if [[ "$donotshowfish" = "donotshowfish" ]]
    then
      echoer "... without showing filtered results"
    fi

  if [[ -s "$licenses_file" ]]
    then
      if [[ $goahead -eq 1 ]]
        then
          if [[ -d "$vendor_licenses_dir" ]]
            then
              echoer "Processing licenses . . ."
              while read line
                do
                  local phish=$(fisher "$line")
                  if [[ 0 -eq "$phish" ]]
                    then
                      if [[ -f "$line" ]]
                        then
                          local filenamepre1=${line////__}
                          local filenamepre2=${filenamepre1// /__}
                          local filename=${filenamepre2//.__/}
                          local hash_output=$(sha256sum "$line")
                          local hash=${hash_output:0:66}
                          local hash_list_len=${#hash_list[@]}
                          if [[ $hash_list_len -eq 0 ]]
                            then
                              cat "$line" > "$vendor_licenses_dir"/"$filename"
                              hash_list[0]=$hash
                              filename_list[0]="$filename"
                              real_files=$(($real_files+1))
                            else
                              local counter=0
                              local match=0
                              for item in ${hash_list[@]}
                                do
                                  if test $item = $hash
                                    then
                                      match=1
                                      break
                                    fi
                                  counter=$(($counter+1))
                                done
                              if [[ $match -eq 0 ]]
                                then
                                  hash_list[$counter]=$hash
                                  filename_list[$counter]="$filename"
                                  cat "$line" > "$vendor_licenses_dir"/"$filename"
                                  real_files=$(($real_files+1))
                                else
                                  cd "$vendor_licenses_dir"
                                  ln -s "${filename_list[$counter]}" "$filename"
                                  linked_files=$(($linked_files+1))
                                  cd "$workingdir"
                                fi
                            fi
                        else
                          echoer "invalid file: $line"
                        fi
                    else
                      if [[ "$donotshowfish" != "donotshowfish" ]]
                        then
                          echoer "no, thanks for the fish$phish : $line"
                        fi
                    fi
                done < $licenses_file
              echoer "License files to install: $real_files"
              echoer "License files to link: $linked_files"
              local size=0
              if [[ 0 -eq $do_no_search ]]
                then
                  local all_files=$(find "$vendor_licenses_dir"/*)
                  for item in $all_files
                    do
                      if [[ -f "$item" ]] || [[ -L "$item" ]]
                        then
                          local file_size=$(stat --format=%s "$item")
                          if [[ $file_size -ne 0 ]]
                            then
                              size=$(($size+$file_size))
                            fi
                        else
                          echoer "invalid item: $item"
                        fi
                    done
                  else
                    echoer "Nothing to search for"
                fi
              local size_string=""
              local sz=$size
              if [[ $size -gt 10000000 ]]
                then
                  sz=$(($size/1000000))
                  size_string="Mega"
                else
                  if [[ $size -gt 10000 ]]
                    then
                      sz=$(($size/1000))
                      size_string="Kilo"
                    fi
                fi
              echoer "Size of licenses to package approximately:
                      $sz $size_string Bytes"
            else
              local e16="invalid directory: $vendor_licenses_dir"
              exit_error 16 "$e16"
            fi
        else
          local e17="Does not package licenses."
          exit_error 17 "$e17"
        fi
    else
      echoer "No files found to package."
    fi
  if [[ "cleanup" = $cleanup ]]
    then
      delete_file $licenses_file
      delete_file $licenses_directories
    fi
  local wdir=$(pwd)
  if [[ "$wdir" != "$workingdir" ]]
    then
      cd "$workingdir"
    fi
}


function installer {
  # Installs or links previously found licenses.
  #
  # $1 set to "runanyway" allows developers to execute outside build environment
  # $2 set to "cleanup" deletes temporary files and directory
  # $3 set to "verbose" gives more results output
  # $4 should be the soure directory, prepared with function "finder"
  # $5 should be the (already created) destination directory

  local knows_the_risk=$1
  local cleanup=$2
  local verbose=$3
  local vendor_licenses_dir=$4
  local install_licenses_dir=$5
  local licenses_files=$(mktemp /tmp/real_license_files_XXXXXXXXXX.txt)
  local licenses_links=$(mktemp /tmp/link_license_files_XXXXXXXXXX.txt)
  local real_files=0
  local linked_files=0
  local goahead=0

  rm $licenses_files
  rm $licenses_links

  if [[ -z "$vendor_licenses_dir" ]] || [[ -z "$install_licenses_dir" ]]
    then
      local e21=" needed arguments:\n
                  1. Source Directory with vendored licenses\n
                  2. Existing Target Directory to install vendored licenses in"
      exit_error 21 "$e21" "lb"
    else
      local both_directories=0
      if [[ ! -d "$vendor_licenses_dir" ]]
        then
          both_directories=$(($both_directories+1))
        fi
      if [[ ! -d "$install_licenses_dir" ]]
        then
          both_directories=$(($both_directories+2))
        fi
      if [[ 0 -eq $both_directories ]]
        then
          find -P "$vendor_licenses_dir" -type f -fprintf $licenses_files "%f\n"
          find -P "$vendor_licenses_dir" -type l -fprintf $licenses_links "%f %l\n"
          goahead=1

          if [[ -s $licenses_files ]]
            then
              declare -A installers
              declare -A linkers
              while read line
                do
                  local file="$vendor_licenses_dir"/"$line"
                  if [[ -f "$file" ]]
                    then
                      local target_file="$install_licenses_dir"/"$line"
                      if [[ -a "$target_file" ]]
                        then
                          echoer "install target already exists: $target_file"
                        else
                          install -D -m 0644 "$file" "$target_file"
                          if [[ $? -eq 0 ]]
                            then
                              real_files=$(($real_files+1))
                              installers["$line"]=0
                            else
                              echoer "failed to install: $line"
                            fi
                        fi
                    else
                      echoer "file to install not found: $file"
                    fi
                done < $licenses_files

              cd "$install_licenses_dir"
              while read line
                do
                  local combo=($line)
                  local combo_len=${#combo[@]}
                  if [[ $combo_len -eq 2 ]]
                    then
                      if [[ -f "${combo[1]}" ]]
                        then
                          if [[ -a "${combo[0]}" ]]
                            then
                              echoer "link target already exists: ${combo[0]}"
                            else
                              ln -s "${combo[1]}" "${combo[0]}"
                              if [[ $? -eq 0 ]]
                                then
                                  linked_files=$(($linked_files+1))
                                  local v1=$((${installers["${combo[1]}"]}+1))
                                  installers["${combo[1]}"]=$v1
                                  linkers["${combo[0]}"]="${combo[1]}"
                                else
                                  echoer "failed to link ${combo[0]}"
                                fi
                            fi
                        else
                          local t5="source to link ${combo[0]} not found: "
                          t5="$t5${combo[1]}"
                          echoer "$t5"
                        fi
                    else
                      echoer "invalid link pair: $line"
                    fi
                done < $licenses_links

              if [[ -n "$verbose" ]]
                then
                  if [[ "$verbose" = "verbose" ]]
                    then
                      local max=0
                      for item in ${!installers[@]}
                        do
                          if [[ ${installers["$item"]} > $max ]]
                            then
                              max=${installers["$item"]}
                            fi
                        done
                      local installers_len=${#installers[@]}
                      local ct=0
                      local c=0
                      local sorted=()
                      while [[ $ct -le $max ]] && [[ $c -lt $installers_len ]]
                        do
                          for item in ${!installers[@]}
                            do
                              if [[ ${installers["$item"]} -eq $ct ]]
                                then
                                  sorted[$c]="$item"
                                  c=$(($c+1))
                                fi
                            done
                          ct=$(($ct+1))
                        done
                      for item in ${sorted[@]}
                        do
                          local t1="installed $item with "
                          t1="$t1"${installers["$item"]}" Links"
                          echoer "$t1"
                          for i in ${!linkers[@]}
                            do
                              if [[ ${linkers["$i"]} = "$item" ]]
                                then
                                  local t2=$(echo linked: "$i"  "->"  "$item")
                                  echoer "$t2"
                                fi
                            done
                        done
                    fi
                fi
            else
              echoer "There is nothing to install."
            fi
        else
          local exiter=0
          local t3=""
          if [[ 1 -eq $both_directories ]] || [[ 3 -eq $both_directories ]]
            then
              t3="not an existing source directory: $vendor_licenses_dir"
              exiter=22
            fi
          if [[ 2 -eq $both_directories ]] || [[ 3 -eq $both_directories ]]
            then
              local t4="not an existing target directory: $install_licenses_dir"
              if [[ -n "$t3" ]]
                then
                  t3="$t3 \n$t4"
                else
                  t3="$t4"
                fi
              if [[ 0 -eq $exiter ]]
                then
                  exiter=23
                else
                  exiter=24
                fi
            fi
          if [[ 0 -ne $exiter ]]
            then
              exit_error $exiter "$t3" "lb"
            else
              if [[ -n "$t3" ]]
                then
                  echoer "$t3"
                fi
            fi
        fi
    fi
  if [[ "cleanup" = $cleanup ]]
    then
      if [[ $goahead -eq 1 ]]
        then
          delete_dir_recursive "$knows_the_risk" "$vendor_licenses_dir"
        fi
      delete_file $licenses_files
      delete_file $licenses_links
    fi
  echoer "Number of license files installed: $real_files"
  echoer "Number of license files linked: $linked_files"
}


function fisher {
  local fish=0
  local file="$1"
  if [[ -n "$file" ]]
    then
      case "$file" in
        *\$\(*)
          fish=3
          ;;
        /*)
          fish=4
          ;;
        */..*)
          fish=5
          ;;
        *../*)
          fish=6
          ;;
        ..*)
          fish=7
          ;;
        *"\\\\"n*)
          fish=8
          ;;
        *\\\n*)
          fish=9
          ;;
        *\`*)
          fish=10
          ;;
        " "*)
          fish=11
          ;;
        *" "/*)
          fish=12
          ;;
        \\*)
          fish=13
          ;;
        *\\*)
          fish=14
          ;;
      esac
    else
      fish=1
    fi
  echo $fish
}


function test_fisher1 {
  # things still reported as "invalid file" should be catched with a new filter
  # in funtion "fisher" if it can be considered "unwanted"
  local lic_file="$1"
  test_list=(
  "/absolute_path/file"
  "../relative1"
  "..a/relative2"
  ".a/..relative3"
  ".a../.relative4"
  ".a/../relative5"
  ".a/..b/relative6"
  ".a/relative7/.."
  ".a/relative8/../c"
  ".a/relative9/.././.."
  ".a/relative10/.c./../d.."
  ".a/relative11/.c./.e./..d"
  "$lic_file"
  "$(echo $lic_file test1)"
  "test2 $(echo $lic_file)"
  "\$(echo $lic_file test3)"
  "\$\(echo $lic_file test4)"
  "$\(echo $lic_file test5)"
  "`echo test10`"
  "\`echo test11\`"
  "\`test12\`"
  "$test13\`"
  "\n1"
  "\\n2"
  "\\\n3"
  "\\\\n4"
  "\\\\\n5"
  "\\\\\\n6"
  "\\\\\\\n7"
  "n11\n"
  "n12\\n"
  "n13\\\n"
  "n21\nn21"
  "n22\\nn22"
  "n23\\\nn23"
  "file\nname"
  "file2\\nname"
  "file3\\\nname"
  " file4/test"
  "  file5/test"
  " "
  " a "
  "b "
  "\backslash1"
  "\\backslash2"
  "\\\backslash3"
  "\\\\backslash4"
  "\\\\\backslash5"
  "\\\\\\backslash6"
  "\\\\\\\backslash7"
  "backslash17\\\\\\\backslash17"
  "backslash16\\\\\\backslash16"
  "backslash15\\\\\backslash15"
  "backslash14\\\\backslash14"
  "backslash13\\\backslash13"
  "backslash12\\backslash12"
  "backslash11\backslash11"
  )

  echo "$(echo $lic_file test41)" >> $lic_file
  echo "test42 $(echo $lic_file)" >> $lic_file
  echo "\$(echo $lic_file test43)" >> $lic_file
  echo "\$\(echo $lic_file test44)" >> $lic_file
  echo "$\(echo $lic_file test45)" >> $lic_file
  echo "`echo test40`" >> $lic_file
  echo "\`echo test51\`" >> $lic_file
  echo " file14/test" >> $lic_file
  echo "  file15/test" >> $lic_file
  echo " " >> $lic_file
  echo "  " >> $lic_file
  echo " a2 " >> $lic_file
  echo "b2 " >> $lic_file
  echo "b3 b3" >> $lic_file
  echo "b4  b4" >> $lic_file
  echo "\ b5" >> $lic_file
  echo "\  b6" >> $lic_file

  for item in ${test_list[@]}
    do
      echo "$item" >> $lic_file
    done
}


function dir_splitter_for_files_section {
  local start="$1"
  local directory="$2"
  local splitter="/"
  local ct=0
  local result=()
  if [[ -z "$start" ]] || [[ -z "$directory" ]]
    then
      local e31="Parameters 'pathstart' and 'path' needed."
      exit_error 31 "$e31"
    else
      if [[ "$start" = "$splitter" ]]
        then
          local e32="'pathstart' should not be root: $splitter"
          exit_error 32 "$e32"
        else
          if [[ "$directory" != /* ]] || [[ "$start" != /* ]]
            then
              local e33="Splitting only done for absolute paths."
              exit_error 33 "$e34"
            else
              if [[ "$directory" == "$start"/* ]]
                then
                  local sfi="$IFS"
                  IFS=$splitter
                  local parts=$(echo -n $directory)
                  IFS="$sfi"
                  local path=""
                  local addit=0
                  for item in $parts
                    do
                      path="$path$splitter$item"
                      if [[ "$path" = "$start" ]] && [[ 0 -eq $addit ]]
                        then
                          addit=1
                        fi
                      if [[ 1 -eq $addit ]]
                        then
                          if [[ "$path" != "$directory" ]]
                            then
                              result[ct]="$path"
                              ct=$(($ct+1))
                            fi
                        fi
                    done
                else
                  if [[ "$directory" = "$start" ]]
                    then
                      local e34="Directories are equal."
                      exit_error 34 "$e34"
                    else
                      local e35="Second directory has to include first."
                      exit_error 35 "$e35"
                    fi
                fi
            fi
        fi
    fi
  if [[ $ct -gt 0 ]]
    then
      for item in ${result[@]}
        do
          echo %dir $item
        done
    fi
}


# unused
function echo_to_file {
  local c=0
  for i in $2
    do
      echo "$i" >> $1
      c=$(($c+1))
    done
  echo $c
}


function items_found {
  local lines=$(wc --lines $2)
  local number=${lines% $2}
  local found=$(($number-$1))
  echo $found
}


function items_finder {
  local target="$2"
  local item="$3"
  local current=$4
  local typ=""
  local letter=""
  local start=''
  local pattern=""
  local commando=""
  local error_text=""
  local current_valid=0
  if [[ -n "$item" ]]
    then
      case "$1" in
        "f")
          commando="file"
          typ="file"
          letter="f"
          start='./*/'
          pattern="-iname $item*"
          if [[ $found_files -eq $current ]]
            then
              current_valid=1
            fi
          ;;
        "d")
          commando="directory"
          typ="directory"
          letter="d"
          start='./*/'
          pattern="-iname $item"
          if [[ $found_dirs -eq $current ]]
            then
              current_valid=2
            fi
          ;;
        "fd")
          commando="filesindir"
          typ="file"
          letter="f"
          start="$item"
          if [[ $found_files -eq $current ]]
            then
              current_valid=3
            fi
          ;;
        *)
          error_text="unknown command"
          ;;
      esac
      if [[ $current_valid -gt 0 ]]
        then
          if [[ -n "$typ" ]] && [[ -n "$letter" ]] \
            && [[ -n "$start" ]] && [[ -n "$commando" ]]
            then
              local runfind=0
              if [[ $current -eq 0 ]] && [[ -n "$target" ]] && [[ ! -s "$target" ]]
                then
                  runfind=1
                else
                  if [[ $current -gt 0 ]] && [[ -s "$target" ]]
                    then
                      runfind=2
                    else
                      error_text="broken parameter"
                    fi
                fi
              if [[ $runfind -gt 0 ]]
                then
                  local f=0
                  find $start -type $letter $pattern -print | sort >> $target
                  f=$(items_found $current $target)
                  local result=$(($current+$f))
                  if [[ $result -ge 0 ]]
                    then
                      local rt=""
                      case $commando in
                        "file")
                          rt="found: $f"
                          found_files=$result
                          ;;
                        "directory")
                          rt="found : $f"
                          found_dirs=$result
                          ;;
                        "filesindir")
                          rt="files added from directory $start : $f"
                          found_files=$result
                          ;;
                      esac
                      echoer "$rt"
                    fi
                else
                  error_text="logic failure"
                fi
            else
              error_text="missing parameter"
            fi
        else
          error_text="parameter quoting failure"
        fi
    else
      error_text="missing item"
    fi
  if [[ -n "$error_text" ]]
    then
      echoer "$error_text in function 'items_finder'."
    fi
}


function echo_disclaimer {
  if [[ $silent -lt 2 ]]
    then
      echo -e $disclaimer
    fi
}


function delete_file {
  if [[ -f "$1" ]]
    then
      echoer "Delete temporary file: $1"
      rm "$1"
    fi
}


function delete_dir_recursive {
  if [[ -d "$2" ]]
    then
      local p=$(user_check "$1")
      if [[ $p -eq 1 ]]
        then
          echoer "Delete temporary directory: $2"
          rm -r "$2"
        else
          echoer "deleting $2 needs special permissions"
        fi
    else
      echoer "not a directory: $2"
    fi
}


function user_check {
  local r=0
  if [[ "$1" = "runanyway" ]]
    then
      r=1
    else
      local username=$(whoami)
      if [[ "$username" = "$build_user" ]]
        then
          r=1
        fi
    fi
  echo $r
}


function echoer {
  if [[ $silent -lt 1 ]]
    then
      echo $1
    fi
}


function exit_error {
  if [[ $silent -lt 2 ]]
    then
      if [[ "lb" = "$3" ]]
        then
          echo -e $2
        else
          echo $2
        fi
    fi
  exit $1
}


function main {
  local arg1=""
  local arg2=""
  local arg3=""
  local arg4=""
  local arg5=""
  local with_disclaimer=""
  local verbose="no"
  local quiet="no"
  local quieter="no"
  local runanyway="no"
  local cleanup="no"
  local showfish="no"
  local tests="testing"
  for i in "$@"
    do
      case "$i" in
        "--nodisclaimer")
          with_disclaimer=" "
          ;;
        "--verbose")
          verbose="verbose"
          ;;
        "--quiet")
          quiet="quiet"
          ;;
        "--quieter")
          quieter="quieter"
          ;;
        "--runanyway")
          runanyway="runanyway"
          ;;
        "--cleanup")
          cleanup="cleanup"
          ;;
        "--test-fisher1")
          tests=$tests"-fisher1"
          ;;
        "--donotshowfish")
          showfish="donotshowfish"
          ;;
        *)
          if [[ -z "$arg1" ]]
            then
              arg1="$i"
            else
              if [[ -z "$arg2" ]]
                then
                  arg2="$i"
                else
                  if [[ -z "$arg3" ]]
                    then
                      arg3="$i"
                    else
                      # unused
                      if [[ -z "$arg4" ]]
                        then
                          arg4="$i"
                        else
                          if [[ -z "$arg5" ]]
                            then
                              arg5="$i"
                            fi
                        fi
                    fi
                fi
            fi
          ;;
      esac
    done
  if [[ "$verbose" = "no" ]]
    then
      if [[ "$tests" = "testing" ]]
        then
          if [[ "$quieter" = "quieter" ]]
            then
              silent=2
            else
              if [[ "$quiet" = "quiet" ]]
                then
                  silent=1
                fi
            fi
          if [[ $silent -gt 0 ]]
            then
              if [[ "$showfish" = "no" ]]
                then
                  showfish="donotshowfish"
                fi
            fi
        fi
    fi
  if [[ $silent -eq 0 ]]
    then
      if [[ "$tests" = "testing" ]]
        then
          if [[ -n "$run_tests" ]]
            then
              tests=$tests"-fisher1"
            fi
        fi
    fi
  local runsomething=""
  if [[ -n "$arg1" ]]
    then
      case "$arg1" in
        "find")
          runsomething="f"
          ;;
        "install")
          runsomething="i"
          ;;
        "dirsplit")
          runsomething="d"
          with_disclaimer="no"
          ;;
        "help")
          runsomething="h"
          ;;
        "version")
          runsomething="v"
          ;;
      esac
    fi
  if [[ -z "$runsomething" ]]
    then
      local t1="Only commands
                'help', 'version', 'find', 'install' and 'dirsplit'
                are recognized."
      exit_error 1 "$t1"
    else
      if [[ $runsomething = "v" ]]
        then
          echo $version
        else
          if [[ -z "$with_disclaimer" ]]
            then
              echo_disclaimer
            fi
          case "$runsomething" in
            "h")
              helper
              ;;
            "f")
              finder $runanyway $cleanup $tests $showfish "$arg2"
              ;;
            "i")
              installer $runanyway $cleanup $verbose "$arg2" "$arg3"
              ;;
            "d")
              dir_splitter_for_files_section "$arg2" "$arg3"
              ;;
          esac
        fi
    fi
}

main "$@"

